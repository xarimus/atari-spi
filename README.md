# 8-bit Atari SPI

An SPI protocol stack for the line of 6502 based 8-bit Atari personal computers using the controller port as a communication bus.
Originally designed and tested on an Atari 1200 XL using SIO2SD from Lotharek and an Arduino Uno development board (Rev3).

The protocol is implemented in four layers, two on the controller (Atari) side, and two on the peripheral side:
1. controller high  Atari Basic
2. controller low   6502 Assembly
3. peripheral low   Arduino
4. peripheral high  Arduino

This project is preimarily focused on the two low level layers (#2 and #3 above), and moreso on the controller/Atari side as ideally the project does not need to be Arduino hardware or even IDE specific, that is just what I have. If anyone wants to implement the protocol on other divices or in other languages, feel free to request a pull.

This project also includes working example implentations of the two high level layers, but the goal of this project is for versatile implementation options.

Progress:

- [ ] Controller low
  - [x] Data out
  - [ ] Data in
- [ ] Peripheral low
  - [ ] Data out
  - [x] Data in

# Hardware
